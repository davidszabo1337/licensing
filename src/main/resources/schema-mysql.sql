DROP TABLE IF EXISTS `license`;

CREATE TABLE `license` (
                        `id`      integer (20)    NOT NULL AUTO_INCREMENT,
                        `key`    varchar(36)   NOT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_hungarian_ci;