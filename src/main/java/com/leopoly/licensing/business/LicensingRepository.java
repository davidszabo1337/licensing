package com.leopoly.licensing.business;

import com.leopoly.licensing.common.entity.LicenseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface LicensingRepository {
    LicenseEntity getLicenseById(int id);

    void saveLicense(LicenseEntity licenseEntity);
}
