package com.leopoly.licensing.business.service;

import com.leopoly.licensing.application.service.LicensingRepositoryService;
import com.leopoly.licensing.business.LicensingRepository;
import com.leopoly.licensing.common.dto.LicenseDto;
import com.leopoly.licensing.common.entity.LicenseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LicensingRepositoryServiceImpl implements LicensingRepositoryService {
    private final LicensingRepository licensingRepository;

    @Autowired
    public LicensingRepositoryServiceImpl(LicensingRepository licensingRepository) {
        this.licensingRepository = licensingRepository;
    }

    @Override
    public LicenseEntity getLicenseById(int id) {
        return licensingRepository.getLicenseById(id);
    }

    @Override
    public void addLicense(LicenseEntity licenseEntity) {
        licensingRepository.saveLicense(licenseEntity);
    }

    @Override
    public void updateLicense(int id, LicenseDto dto) {
        LicenseEntity licenseEntity = getLicenseById(id);
        licenseEntity.setKey(dto.getKey());
        licensingRepository.saveLicense(licenseEntity);
    }
}
