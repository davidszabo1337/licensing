package com.leopoly.licensing.infrastructure.config;

import com.leopoly.licensing.utility.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    public Mapper mapper(){
        return new Mapper();
    }
}
