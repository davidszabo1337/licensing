package com.leopoly.licensing.common.dto;

import java.util.Objects;

public class LicenseDto {
    private String key;

    public LicenseDto() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LicenseDto that = (LicenseDto) o;
        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        return "LicenseDto{" +
                "key='" + key + '\'' +
                '}';
    }
}
