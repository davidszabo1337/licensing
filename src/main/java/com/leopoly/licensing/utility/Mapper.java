package com.leopoly.licensing.utility;

import com.leopoly.licensing.common.dto.LicenseDto;
import com.leopoly.licensing.common.entity.LicenseEntity;

public class Mapper {
    public Mapper() {
    }
    public LicenseDto mapLicenseEntityToLicenseDto(LicenseEntity licenseEntity) {
        LicenseDto dto = new LicenseDto();
        dto.setKey(licenseEntity.getKey());
        return dto;
    }
    public LicenseEntity mapLicenseDtoToLicenseEntity(LicenseDto dto) {
        LicenseEntity licenseEntity = new LicenseEntity();
        licenseEntity.setKey(dto.getKey());
        return  licenseEntity;
    }
}
