package com.leopoly.licensing.application.service;

import com.leopoly.licensing.common.dto.LicenseDto;
import com.leopoly.licensing.common.entity.LicenseEntity;
import org.springframework.stereotype.Service;

@Service
public interface LicensingRepositoryService {
    LicenseEntity getLicenseById(int id);

    void addLicense(LicenseEntity licenseEntity);

    void updateLicense(int id, LicenseDto dto);
}
