package com.leopoly.licensing.application.service;

import com.leopoly.licensing.common.dto.LicenseDto;
import com.leopoly.licensing.common.entity.LicenseEntity;
import com.leopoly.licensing.utility.Mapper;
import com.leopoly.licensing.web.service.LicensingApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LicensingApplicationServiceImpl implements LicensingApplicationService {
    private final LicensingRepositoryService licensingRepositoryService;
    private final Mapper mapper;

    @Autowired
    public LicensingApplicationServiceImpl(LicensingRepositoryService licensingRepositoryService, Mapper mapper) {
        this.licensingRepositoryService = licensingRepositoryService;
        this.mapper = mapper;
    }

    @Override
    public LicenseDto getLicenseById(int id) {
        LicenseEntity licenseEntity = licensingRepositoryService.getLicenseById(id);
        return mapper.mapLicenseEntityToLicenseDto(licenseEntity);
    }

    @Override
    public void addLicense(LicenseDto dto) {
        LicenseEntity licenseEntity = mapper.mapLicenseDtoToLicenseEntity(dto);
        licensingRepositoryService.addLicense(licenseEntity);
    }

    @Override
    public void updateLicense(int id, LicenseDto dto) {
        licensingRepositoryService.updateLicense(id, dto);
    }
}
