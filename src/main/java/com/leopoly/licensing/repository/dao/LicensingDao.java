package com.leopoly.licensing.repository.dao;

import com.leopoly.licensing.common.entity.LicenseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicensingDao extends JpaRepository<LicenseEntity, Integer> {
}
