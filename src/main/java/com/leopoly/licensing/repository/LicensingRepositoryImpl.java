package com.leopoly.licensing.repository;

import com.leopoly.licensing.business.LicensingRepository;
import com.leopoly.licensing.common.entity.LicenseEntity;
import com.leopoly.licensing.repository.dao.LicensingDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class LicensingRepositoryImpl implements LicensingRepository {
    private final LicensingDao licensingDao;

    @Autowired
    public LicensingRepositoryImpl(LicensingDao licensingDao) {
        this.licensingDao = licensingDao;
    }

    @Override
    public LicenseEntity getLicenseById(int id) {
        return licensingDao.getById(id);
    }

    @Override
    public void saveLicense(LicenseEntity licenseEntity) {
        licensingDao.saveAndFlush(licenseEntity);
    }
}
