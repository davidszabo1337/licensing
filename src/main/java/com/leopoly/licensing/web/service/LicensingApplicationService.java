package com.leopoly.licensing.web.service;

import com.leopoly.licensing.common.dto.LicenseDto;
import org.springframework.stereotype.Service;

@Service
public interface LicensingApplicationService {
    LicenseDto getLicenseById(int id);

    void addLicense(LicenseDto dto);

    void updateLicense(int id, LicenseDto dto);
}
