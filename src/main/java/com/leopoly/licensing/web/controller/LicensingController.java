package com.leopoly.licensing.web.controller;

import com.leopoly.licensing.common.dto.LicenseDto;
import com.leopoly.licensing.web.service.LicensingApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/license-service")
public class LicensingController {

    private final LicensingApplicationService licensingApplicationService;

    @Autowired
    public LicensingController(LicensingApplicationService licensingApplicationService) {
        this.licensingApplicationService = licensingApplicationService;
    }

    @GetMapping(value = {"v1.0/license/{id}"}, produces = "application/json")
    public ResponseEntity<LicenseDto> getLicense(@PathVariable int id) {
        LicenseDto dto = licensingApplicationService.getLicenseById(id);
        return ResponseEntity
                .ok()
                .body(dto);
    }

    @PostMapping(value = {"v1.0/license"}, produces = "application/json")
    public ResponseEntity<LicenseDto> addLicense(@RequestBody LicenseDto dto) {
        licensingApplicationService.addLicense(dto);
        return ResponseEntity
                .ok()
                .build();
    }

    @PutMapping(value = {"v1.0/license/{id}"}, produces = "application/json")
    public ResponseEntity<LicenseDto> updateLicense(@PathVariable(value = "id") int id,
                                                    @RequestBody LicenseDto dto) {
        licensingApplicationService.updateLicense(id, dto);
        return ResponseEntity
                .ok()
                .build();
    }
}